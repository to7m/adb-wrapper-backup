#!/bin/env python3


from subprocess import check_output, run
from pathlib import Path
import re
from itertools import chain
from datetime import datetime


blacklist = (# these seem to not contain media
             "/acct", "/cache", "/dev", "/dsp", "/firmware", "/proc", "/sys",
             "/system", "/vendor",

             # these are just top level system files
             "/init", "/init.environ.rc", "/init.rc", "/init.usb.configfs.rc",
             "/init.usb.rc", "/init.zygote32.rc", "/init.zygote64_32.rc",
             "/ueventd.rc",

             # duplications of data, maybe due to hard links
             "/mnt/runtime" "/storage/emulated",

             # metadata, probably not essential
             "/data/media/0/DCIM/.thumbnails")


blacklisted_paths = {Path(path_str) for path_str in blacklist}
unsafe_parents = set(chain.from_iterable(
                         path.parents for path in blacklisted_paths))


target_dir_name = f"phone_backup_{datetime.now().isoformat()}"
target = Path.cwd() / target_dir_name
target.mkdir()

run("adb root".split(' '))


def ls_children(path):
    command = f"adb shell ls {path}"
    output = check_output(command.split(' ')).decode()
    name_lines = (line for line in output.split('\n') if line)
    child_paths = [path / name for name in name_lines]
    return child_paths


def ls_recursive_of_type(path, type):
    command = f"adb shell find {path} -type {type}"
    output = check_output(command.split(' ')).decode()
    paths = (Path(line) for line in output.split('\n') if line)
    return paths


def phone_to_pc(path):
    return Path(f"{target}/{str(path)[1:]}")


def phone_to_pc_relative_str(path):
    return f"{target_dir_name}{path}"


def pull_file(path):
    command = f"adb pull {path} {phone_to_pc_relative_str(path)}"
    run(command.split(' '))


def copy_safe_path(path):
    dirs = ls_recursive_of_type(path, 'd')
    files = ls_recursive_of_type(path, 'f')
    for dir_ in dirs:
        target_dir = phone_to_pc(dir_)
        target_dir.mkdir()
    for file in files:
        pull_file(file)


def traverse_unsafe_path(path):
    for child_path in ls_children(path):
        if child_path in blacklisted_paths:
            continue
        elif child_path in unsafe_parents:
            target_dir = phone_to_pc(child_path)
            target_dir.mkdir()
            traverse_unsafe_path(child_path)
        else:
            copy_safe_path(child_path)


traverse_unsafe_path(Path('/'))
